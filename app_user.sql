/*
 Navicat Premium Data Transfer

 Source Server         : MySQL
 Source Server Type    : MySQL
 Source Server Version : 100137
 Source Host           : localhost:3306
 Source Schema         : keuangan_db

 Target Server Type    : MySQL
 Target Server Version : 100137
 File Encoding         : 65001

 Date: 03/07/2019 12:33:08
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for app_user
-- ----------------------------
DROP TABLE IF EXISTS `app_user`;
CREATE TABLE `app_user`  (
  `user_id` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `user_name` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `user_password` varchar(32) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `user_realname` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_st` tinyint(1) NOT NULL COMMENT '1:active,0:not-active',
  `user_photo` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_group` int(2) NOT NULL COMMENT '1:administrator,2:publisher,3:creator',
  `user_description` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `last_login` datetime(0) NULL DEFAULT NULL,
  `st_login` tinyint(1) NULL DEFAULT NULL COMMENT '1:active,0:not-active',
  `is_pegawai` tinyint(1) NOT NULL,
  `pasar_id` char(6) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of app_user
-- ----------------------------
INSERT INTO `app_user` VALUES ('5', 'admin', '77e2edcc9b40441200e31dc57dbb8829', 'Administrator', 1, '', 2, 'Administrator', '2019-06-24 15:03:31', 1, 0, NULL);
INSERT INTO `app_user` VALUES ('6', 'petanahan', '06316e96fa229b361b44cc65e8482434', 'Operator Pasar Petanahan', 1, 'disperindag.kebumenkab.go.id.220319-petanahan.png', 2, '', '2019-05-24 20:37:14', 0, 0, '200006');
INSERT INTO `app_user` VALUES ('7', 'prembun', 'bc6c7014349bad8797ba60e9d5310a10', 'Operator Pasar Prembun', 0, 'disperindag.kebumenkab.go.id.220319-prembun.png', 2, '', '2019-04-08 19:11:04', 0, 0, '100001');
INSERT INTO `app_user` VALUES ('14', 'alfian', '2d30815bff30177260011392be6c5a9c', 'Alfian Muhammad Ardianto', 1, 'simbok.kebumenkab.go.id.290519-alfian.png', 2, '', '2019-05-30 08:19:23', 0, 0, NULL);
INSERT INTO `app_user` VALUES ('15', 'developer', '5c1b29014cd1f34ec326aa6242d41cf1', 'Developer Web', 1, 'simbok.kebumenkab.go.id.300519-developer.png', 1, '', '2019-07-03 08:22:40', 1, 0, NULL);
INSERT INTO `app_user` VALUES ('12', 'superadmin', '5994d324f32285147b2f738fb10141ab', 'Superadmin', 1, 'simbok.kebumenkab.go.id.280519-superadmin.png', 2, '', '2019-05-31 09:01:08', 0, 0, NULL);

SET FOREIGN_KEY_CHECKS = 1;
